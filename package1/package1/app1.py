#!/usr/bin/env python3

import logging
import foo

logger = logging.getLogger()
# logger = logging.getLogger('log_example')
# logger = logging.getLogger('bar ')
# logger = logging.getLogger('root')

logger.setLevel(logging.INFO)
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


def main():
    logger.info('Info from main app1')
    logger.info("__name__ = " + __name__)
    foo.foo_func()


if __name__ == "__main__":
    main()
