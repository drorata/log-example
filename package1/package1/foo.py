import logging

logger = logging.getLogger(__name__)


def foo_func():
    logger.info("__name__ = " + __name__)
    logger.info('Info from foo')
