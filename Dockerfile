FROM python:3.6-slim

COPY package1/ ${HOME}/package1/
WORKDIR ${HOME}/package1
RUN pip install .

COPY package2/ ${HOME}/package2/
WORKDIR ${HOME}/package2
RUN pip install .
