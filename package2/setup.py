from setuptools import setup, find_packages

setup(
    name='package2',
    version='0',
    author='Dror Atariah',
    author_email='drorata@gmail.com',
    packages=find_packages()
)
