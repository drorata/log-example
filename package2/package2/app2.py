#!/usr/bin/env python3

import logging
import package1.foo as foo


def main():
    logger = logging.getLogger()
    # logger = logging.getLogger('log_example')

    logger.setLevel(logging.INFO)
    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)
    formatter = logging.Formatter(
        '%(levelname)s <-> %(asctime)s - %(name)s - %(message)s')
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    logger.info('Info from main app2')
    logger.info("__name__ = " + __name__)
    foo.foo_func()


if __name__ == "__main__":
    main()
